var d3 = require('d3')

PieChart.defaultSettings = {
	"label" : "category",
	"value" : "value"
};

PieChart.settings = EnebularIntelligence.SchemaProcessor([
{
  type : "key", name : "label", help : "Please specify the key of the data to be the label."
},{
  type : "key", name : "value", help : "Please specify the key of the data representing the value."
}
]);

function PieChart(settings, options) {
	var that = this;
	this.el = window.document.createElement('div');
	var width = options.width || 680;
	var height = options.height || 450;
	that.radius = Math.min(width, height) / 2 - 40;

	this.settings = settings
	this.pieChartWidth = 585;
	this.pieChartHeight = 295;

	this.scaleRatio = 1;
	this.color_list = ["#70C1B3","#247BA0","#FFE066","#F25F5C","#50514F","#F45B69","#211103","#5C8001","#23395B","#470063"]; 
	this.options = options;

	this.svg = d3.select(this.el)
		.append("svg")
		.attr('class', 'svgWrapper');

	this.base = this.svg
		.attr('width', width)
		.attr('height', height)
		.append("g")
		.attr('class', 'piechart');

	this.base.append("g")
		.attr("class", "piechart__lines");
	this.base.append("g")
		.attr("class", "piechart__slices");
	this.base.append("g")
		.attr("class", "piechart__labels");

	this.pie = d3.layout.pie()
		.sort(null)
		.value(function(d) {
			return that.getValue(d);
		});

	this.arc = d3.svg.arc()
		.outerRadius(that.radius * 0.8)
		.innerRadius(that.radius * 0.4);

	this.outerArc = d3.svg.arc()
		.innerRadius(that.radius * 0.9)
		.outerRadius(that.radius * 0.9);

	this.base.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

	this.data = [];

	this.setSchema(settings);
}

/*
PieChart.prototype.onDrillDown = function(cb) {
}
*/

PieChart.prototype.addData = function(data) {
    var that = this;

    function fireError(err){
      if(that.errorCallback){
            that.errorCallback({
              error: err
            })
      }
    }

    if(data instanceof Array) {
    var label = this.settings.label
    var value = this.settings.value

    this.data = data
      .filter(d => {
          let hasLabel = d.hasOwnProperty(label)
          const dLabel = d[label]
          if(typeof dLabel !== 'string' && typeof dLabel !== 'number' ){
              fireError('label is not a string or number')
              hasLabel = false
          }
          return hasLabel
       })
      .filter(d => {
          let hasValue = d.hasOwnProperty(value)
          if(typeof d[value] !== 'number'){
            fireError('value is not a number')
            hasValue = false
          }

          return hasValue
       })
        this.refresh();
    }

}

PieChart.prototype.clearData = function() {
	this.data = [];
	this.refresh();
}

PieChart.prototype.calculate = function() {
	var that = this;
	var newdata = {};

	this.data.forEach(function(d,i) {
		var k = d[that.schema.label];
		if(!newdata[k]) newdata[k] = 0;
		newdata[k] += Number(d[that.schema.value]);
	});

	return Object.keys(newdata).map(function(k) {
		return {
			key : k,
			value : newdata[k]
		}
	}).sort(function(a, b){
            if(a.key < b.key) return -1;
            if(a.key > b.key) return 1;
            return 0;
    })
}


PieChart.prototype.setSchema = function(schema) {
	this.schema = schema;
}

PieChart.prototype.getLabel = function(d) {
	return d.data.key;
}

PieChart.prototype.getValue = function(d) {
	return d.value;
}

PieChart.prototype.refresh = function() {
	this.change( this.calculate() );
}

PieChart.prototype.resize = function(options) {

	this.radius = Math.min(options.width, options.height) / 2 - 40;

	var margin = {
		top: 30,
		left: 30,
		bottom: 30,
		right: 30
	};

	var xRatio = (options.width - margin.left - margin.right) / this.pieChartWidth;
	var yRatio = (options.height - margin.top - margin.bottom) / this.pieChartHeight;

	this.scaleRatio = (xRatio > yRatio) ? yRatio : xRatio;
	if(this.scaleRatio > 1) this.scaleRatio = 1;

	var translate = {
		x: options.width/2/this.scaleRatio ,
		y: options.height/2/this.scaleRatio
	}

	this.arc = d3.svg.arc()
    .outerRadius(this.radius * 0.8)
    .innerRadius(this.radius * 0.4);

  this.outerArc = d3.svg.arc()
    .innerRadius(this.radius * 0.9)
    .outerRadius(this.radius * 0.9);

	this.base
	.attr("width", options.width)
	.attr("height", options.height)
	.transition().duration(500)
	.attr("transform", "scale(" + this.scaleRatio + "," + this.scaleRatio + ") translate(" + translate.x + "," + translate.y + ")");
}

PieChart.prototype.change = function(data) { 
	var that = this; 
	var key = function(d){ 
		return that.getLabel(d); 
	}; 

	/* ------- PIE SLICES -------*/
	var slice = this.base.select(".piechart__slices").selectAll("path.piechart__slice")
		.data(this.pie(data), key);

	slice.enter()
		.insert("path")
		.style("fill", function(d,i) { return that.color_list[i]})
		.attr("class", "piechart__slice");

	slice
		.transition().duration(1000)
		.attrTween("d", function(d) {
			this._current = this._current || d;
			var interpolate = d3.interpolate(this._current, d);
			this._current = interpolate(0);
			return function(t) {
				return that.arc(interpolate(t));
			};
		})

	slice.exit()
		.remove();

	/* ------- TEXT LABELS -------*/

	var text = this.base.select(".piechart__labels").selectAll("text")
		.data(that.pie(data), key);

	var textEnter = text.enter().append("g")

	textEnter
		.append("text")
		.attr("dy", ".35em")
		.text(function(d) {
			return that.getLabel(d);
		})
		.attr("font-family",'Arial, Helvetica, sans-serif')

	var textValue = textEnter.append("text") 
		.attr("dy", "1.5em") 
		.attr("font-family",'Arial, Helvetica, sans-serif')

	textValue 
		.text(function(d) { 
			return d.data.value.toFixed(2); 
		}); 

	function midAngle(d){
		return d.startAngle + (d.endAngle - d.startAngle)/2;
	}

	text.transition().duration(1000)
		.attrTween("transform", function(d) {
			this._current = this._current || d;
			var interpolate = d3.interpolate(this._current, d);
			this._current = interpolate(0);
			return function(t) {
				var d2 = interpolate(t);
				var pos = that.outerArc.centroid(d2);
				pos[0] = that.radius * (midAngle(d2) < Math.PI ? 1 : -1);
				return "translate("+ pos +")";
			};
		})
		.styleTween("text-anchor", function(d){
			this._current = this._current || d;
			var interpolate = d3.interpolate(this._current, d);
			this._current = interpolate(0);
			return function(t) {
				var d2 = interpolate(t);
				return midAngle(d2) < Math.PI ? "start":"end";
			};
		});

	text.exit()
		.remove();

	/* ------- SLICE TO TEXT POLYLINES -------*/

	var polyline = this.base.select(".piechart__lines").selectAll("polyline")
		.data(this.pie(data), key);

	polyline.enter()
		.append("polyline");

	polyline.transition().duration(1000)
		.attrTween("points", function(d){
			this._current = this._current || d;
			var interpolate = d3.interpolate(this._current, d);
			this._current = interpolate(0);
			return function(t) {
				var d2 = interpolate(t);
				var pos = that.outerArc.centroid(d2);
				pos[0] = that.radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
				return [that.arc.centroid(d2), that.outerArc.centroid(d2), pos];
			};
		});

	polyline.exit()
		.remove();
};

PieChart.prototype.onError = function(errorCallback) {
  this.errorCallback = errorCallback
}

PieChart.prototype.getEl = function() {
	return this.el;
}

PieChart.prototype.createDialog = function() {
	return {
		properties : [{
			name : 'value',
			type : 'property'
		}]
	}
}

window.EnebularIntelligence.register('piechart', PieChart);

module.exports = PieChart;